#!/bin/bash

# Graceful Exit of Script to release the GPIO pin
trap printout SIGINT
printout() {
    echo "Script Terminiated: Releasing GPIO Pin"
    echo $gpio_pin > /sys/class/gpio/unexport
    exit
}

# CLI Parameters
gpio_pin=${gpio_pin:-0}
on=${on:-5}
off=${off:-5}
# Search for and set CLI Parameters as variables
while [ $# -gt 0 ]; do
   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
        #echo $1 $2 # Optional to see the parameter:value result
   fi
  shift
done

# Vars
gpio_pin_str=gpio${gpio_pin}
control_on="out"
control_off="in"

# Check if GPIO pin was supplied
if [ "$gpio_pin" == "0" ]; then
    echo "Must supply --gpio_pin"
else
    # TODO: check if GPIO already in use

    # Setup to control pin as output
    echo ${gpio_pin} > /sys/class/gpio/export

    # infinate loop
    while true
    do

        echo "On for ${on}"
        # Set Direction
	echo ${control_on} > /sys/class/gpio/${gpio_pin_str}/direction
        # sleep
        sleep ${on}

        echo "Off for ${off}"
        # Set Direction
        echo ${control_off} > /sys/class/gpio/${gpio_pin_str}/direction

        # sleep
        sleep ${off}

    done

    #Setup to read pin as an input
    #echo "23" > /sys/class/gpio/export
    #echo "in" > /sys/class/gpio/${gpio_pin_str}/direction

    #Read its state
    #cat /sys/class/gpio/${gpio_pin_str}/value
fi
