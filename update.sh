source scripts/apt-update.sh
source scripts/git-update.sh
source scripts/composer-update.sh

# track update completed
UPDATE_COMPLETED=update-completed
touch $UPDATE_COMPLETED
