#!/bin/bash

# run from user's home folder
cd ~

SETUP_COMPLETED=setup-completed
if test -f "$SETUP_COMPLETED"; then
    echo "$SETUP_COMPLETED exists."
else
    # System wide updates
    sudo apt update -y
    sudo apt upgrade -y
    sudo apt dist-upgrade

    # cleanup
    sudo apt autoremove -y
    sudo apt autoclean
    sudo apt clean

    #sudo rpi-update
    #sudo reboot

    # Basic Installs
    sudo apt-get install -y apache2 php phpunit php-xml php-curl git openssl shellinabox

    # Enable Service(s)
    sudo service shellinabox enable

    # Configure GIT
    git config --global user.email "fungifruit@fungifruit.local"
    git config --global user.name "Fungi Fruit"
    
    # Setup Composer
    wget -O composer-setup.php https://getcomposer.org/installer
    sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
    composer --version
    sudo rm -rf composer-setup.php
    sudo composer self-update

    # Setup Laravel Web Instance

    # Clone the Grow Project
    git clone https://gitlab.com/Fungi-Fruit/Grow.git
    pushd Grow
    git pull

    # enter directory
    pushd ../laravel-app

    # update composer
    composer update

    # create .env
    cp .env.fungifruit-example .env
    php artisan key:generate

    # prepare database
    touch database/database.sqlite

    # set permissions
    chmod -R 777 storage bootstrap/cache

    # Return to Directory
    popd

    # apache2

    # enable mod rewrite
    sudo a2enmod rewrite

    # copy laravel.conf
    sudo cp laravel.conf /etc/apache2/sites-available/laravel.conf
    # enable site
    sudo a2ensite laravel.conf

    #restart apache service
    sudo systemctl restart apache2

    # return to directory
    popd

    touch $SETUP_COMPLETED
fi
